#[get("/")]
fn index() -> &'static str {
    "Navigate to http://localhost:8080/check/<type your Coin> to check details about this Coin"
}

#[get("/check/<coin>")]
fn check(coin: &RawStr) -> Result<String, Box<dyn std::error::Error>> {
    let request_url =format!("https://api.coingecko.com/api/v3/coins/{}", coin);
    println!("{}", request_url);
    let resp = reqwest::blocking::get(request_url)?;
    if resp.status().is_success(){

        `Ok(format!("{} ", resp.text().unwrap()))`
    }
    else{
        let response = resp.text().unwrap();
        Ok(format!("{} is not a coin!", response))
    }
}

fn main() {
    rocket::ignire().mount("/", routes![index, check]).launch();
}
